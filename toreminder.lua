#!/usr/bin/env lua
local cal     = require "cal"
local rem     = require "rem"
local common  = require "common"

common.set_verbosity (1)

local main = function (argv)
  local ok, parsed = cal.parse_stdin ()
  if not ok then
    return -1
  end

  local ok = rem.output_calendar (parsed)
  print ""

  return ok and 0 or -1
end

os.exit (main ())

