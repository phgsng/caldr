description
-------------------------------------------------------------------------------

*Caldr*: read / write icalendar (RFC 2445) files.

Converts between ics and the remind(1) format.

dependencies
-------------------------------------------------------------------------------

- A Lua interpreter; tested on 5.3;
- *lpeg*;
- *cjson*;
- *luaposix* (optional).

See ``misc/nix-lua-shell.nix`` for nix script that builds a working
environment.

