#!/usr/bin/env lua
local rem               = require "rem"
local db                = require "db"
local common            = require "common"

local status            = common.status
local status_ok         = status.codes.ok
local status_error      = status.codes.error

common.set_verbosity (1)

local main = function (argv)
  local st, parsed = db.read_json_db ()
  if st ~= status_ok then
    return -1
  end

  local st = rem.output_calendar (parsed)

  return st == status_ok and 0 or -1
end

os.exit (main ())

