with import <nixpkgs> {};
with lua53Packages;

let
  libs = [lua5_3 lua53Packages.lpeg];
in
stdenv.mkDerivation rec {
  name = "lua-env";
  buildInputs = libs;

  shellHook = ''
    export LUA_CPATH="${lib.concatStringsSep ";" (map getLuaCPath libs)};./?.so"
    export LUA_PATH="${lib.concatStringsSep ";" (map getLuaPath libs)};./?.lua"
  '';
}

