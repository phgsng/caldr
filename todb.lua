#!/usr/bin/env lua
local cal     = require "cal"
local db      = require "db"
local common  = require "common"

common.set_verbosity (1)

local main = function (argv)
  local ok, parsed = cal.parse_stdin ()
  if not ok then
    return -1
  end

  local ok = db.output_calendar (parsed)

  return ok and 0 or -1
end

os.exit (main ())

