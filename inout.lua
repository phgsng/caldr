#!/usr/bin/env lua
local cal = require "cal"

local main = function (argv)
  local ok, parsed = cal.parse_stdin ()
  if not ok then
    error ("error parsing input: " .. parsed)
    return -1
  end

  cal.output (parsed)

  return 0
end

os.exit (main ())

