--[[--

  JSON file database.

--]]--

local inspect = require "ext.inspect"

local cjson               = require "cjson"

local pcall               = pcall

local io                  = require "io"
local iostdout            = io.stdout
local iostdin             = io.stdin

local common              = require "common"
local common_status       = common.status
local status_ok           = common_status.codes.ok
local status_error        = common_status.codes.error
local println             = common.println
local errorln             = common.errorln
local noiseln             = common.noiseln
local debugln             = common.debugln

local json_internalize = function (jsdata)
  local jsdec = cjson.new ()
  local parsed = jsdec.decode (jsdata)
  if parsed == nil then return status_error end
  return status_ok, parsed
end

-------------------------------------------------------------------------------
--- API
-------------------------------------------------------------------------------

local read_json_db = function (cal, fh)
  fh = fh or iostdin
  local jsdata = fh:read "*all"
  fh:close ()
  if jsdata == nil then return status_error end
  return json_internalize (jsdata)
end

local output_calendar = function (cal, fh)
  fh = fh or iostdout
  local jsenc = cjson.new ()
  local ok, str = pcall (jsenc.encode, cal)
  if ok == false then
          errorln ("todb: failed to serialize calendar: %s", str)
    return status_error
  end
  fh:write (str)
  return status_ok
end

return { output_calendar = output_calendar
       , read_json_db    = read_json_db
       }

